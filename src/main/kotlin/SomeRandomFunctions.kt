package cat.itb.m05.uf2

import kotlin.math.truncate


/**
 * Selects a list of the numbers between the two parameters introduced
 * including the two first parameters
 * @param value1 first value
 * @param value2 second value
 * @returns a list of numbers as an Int List
 */
fun numbersBetween(value1: Int, value2: Int): List<Int> {
    val numbersList = mutableListOf<Int>()
    for (i in value1..value2) {
        numbersList.add(i)
    }
    return numbersList
}


/**
 * Finds the minimum value of a list
 * @param valueList a list of numbers
 * @return the minimum value as Int
 */
fun findMinValue(valueList: List<Int>): Int {
    var minValue = valueList[0]
    for (i in valueList.indices) {
        if (valueList[i] <= minValue) {
            minValue = i
        }
    }
    return minValue
}


/**
 * Selects the result of the exam correspondent to their mark
 * @param mark the mark of the student
 * @return the result that the student obtained as String
 * @
 */
fun markSelector(mark: Int): String {
    val answer =  when (mark) {
        1, 2, 3, 4 ->  "Insuficient"
        0 ->  "No presentat"
        10 ->  "Excelent + MH"
        5, 6 ->  "Suficient"
        7 ->  "Notable"
        else ->  "No valid"
    }
    return answer
}


/**
 * Finds the position of a character inside a word given
 * @param word the word given
 * @param character the character desired
 * @return a list of the positions of the character in the word given as an Int List
 */
fun charPositionsInString(word: String, character: Char): List<Int> {
    val positionList = mutableListOf<Int>()
    for (i in word.indices) {
        if (word[i] == character) {
            positionList.add(i)
        }
    }
    return positionList
}


/**
 * Finds the first appearance of the desired character in the word given
 * In case the character isn't in the word the position would be -1
 * @param word the word given
 * @param character the character desired
 * @return the character position as Int
 */
fun firstAppearancePosition(word: String, character: Char): Int {
    var position = -1
    for (i in word.indices) {
        if (word[i] == character) {
            position = i
            break
        }
    }
    return position
}


/**
 * Selects the rounded mark of the mark introduced while the mark has passed the 5
 * @param grade the grade mark of the student
 * @return the rounded mark as Int
 * @exception grade is lower than 5 the mark will we rounded to the lowest
 */
fun finalMark(grade: Double): Int {
    if(grade<5){
        truncate(grade)
    }
    return grade.toInt()
}