import cat.itb.m05.uf2.*
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
internal class SomeRandomFunctionsKtTest {

    @Test
    fun numbersBetweenTest() {
        val value1 = 1
        val value2 = 4
        val expected = "[1, 2, 3, 4]"
        val toCheck = "${numbersBetween(value1,value2)}"
        assertEquals(expected,toCheck)
    }

    @Test
    fun minValueTest() {
        val valuesList = listOf(5, -3, 2, 111, 1, 10, 13)
        val expected = -3
        val toCheck = findMinValue(valuesList)
        assertEquals(expected,toCheck)
    }

    @Test
    fun markSelectorTest() {
        val expected = "Suficient"
        val toCheck = markSelector(5)
        assertEquals(expected,toCheck)
    }

    @Test
    fun charPositionsInStringTest() {
        val expected = "[1, 3, 5]"
        val toCheck = "${charPositionsInString("Patata",'a')}"
        assertEquals(expected,toCheck)
    }

    @Test
    fun firstAppearancePositionTest() {
        val expected = 1
        val toCheck = firstAppearancePosition("Maria",'a')
        assertEquals(expected,toCheck)
    }

    @Test
    fun finalMarkPassedTest() {
        val expected = 7
        val toCheck = finalMark(7.5)
        assertEquals(expected,toCheck)
    }
    @Test
    fun finalMarkFailedTest(){
        val expected = 4
        val toCheck = finalMark(4.9)
        assertEquals(expected,toCheck)
    }
}